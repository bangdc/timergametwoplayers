package main;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppMVC extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {

		Model model = new Model();
		View view = new View();
		Controller controller = new Controller(view, model);
		controller.init();

		Scene scene = new Scene(view.getRoot());
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
