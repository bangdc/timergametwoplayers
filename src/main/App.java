package main;

import java.util.Random;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {
	private static int TIME_CONSTRAINT = 5000;

	private BorderPane root;

	private HBox top;
	private VBox left;
	private HBox right;
	private HBox center;
	private HBox bottom;

	private ProgressBar timer;
	private Label timerLabel;
	private Button btnStart;
	private Button btnPause;
	private Button btnContinue;
	private Button btnSubmit;
	private TextField inputTextField;
	private Label notification;
	private VBox rightTurnIndex;
	private VBox rightPlayerInput;
	private VBox rightComputerInput;
	private VBox rightSum;

	public class GameState {
		private boolean running;

		public GameState() {
			running = true;
		}

		public boolean isRunning() {
			return running;
		}

		public synchronized void setRunning(boolean running) {
			this.running = running;
		}
	}

	private GameState gameState;

	private volatile int progress = 0;
	private volatile boolean USER_MISSED_TURN = false;

	private volatile int playerLastInput;
	private volatile int computerLastInput;

	Task<Void> timerTask = new Task<Void>() {

		@Override
		protected Void call() throws Exception {
			while (true) {
				int progressSelf = progress;
				updateProgress(progressSelf, 100);
				updateMessage(String.valueOf(progressSelf * TIME_CONSTRAINT));
			}
		}
	};

	Task<Void> gameManager = new Task<Void>() {
		Random random = new Random();

		@Override
		protected Void call() throws Exception {
			int turnIndex = 1;
			int currentPlayer = 1;

			while (true) {
				if (currentPlayer == 1) {
					System.out.println("Turn #" + turnIndex);
					int currentTurnIndex = turnIndex;
					Platform.runLater(new Runnable() {

						@Override
						public void run() {
							rightTurnIndex.getChildren().add(new Label(String.valueOf(currentTurnIndex)));
						}
					});
				}

				Thread timer = new Thread(new Runnable() {

					@Override
					public void run() {
						for (int i = 100; i >= 0; i--) {
							synchronized (gameState) {
								while (!gameState.isRunning()) {
									try {
										gameState.wait();
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
								}
							}

							progress = i;
							try {
								Thread.sleep(TIME_CONSTRAINT / 100);
							} catch (InterruptedException e) {
								progress = 0;
								break;
							}
						}
						System.out.println("TIME'S UP");
					}
				});

				if (currentPlayer == 2) {
					USER_MISSED_TURN = true;

					Platform.runLater(new Runnable() {

						@Override
						public void run() {
							notification.setText("Current player is Player");
							inputTextField.setDisable(false);
							btnSubmit.setDisable(false);
						}
					});

					Thread userTurnTask = new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								Thread.sleep(TIME_CONSTRAINT);
							} catch (InterruptedException e) {
								return;
							}
							if (USER_MISSED_TURN) {
								System.out.println("Missing");
								playerLastInput = 0;
								Platform.runLater(new Runnable() {

									@Override
									public void run() {

										rightPlayerInput.getChildren().add(new Label("Missed - 0"));
									}
								});
							}
						}
					});
					userTurnTask.setDaemon(true);
					userTurnTask.start();

					btnSubmit.setOnMouseClicked(new EventHandler<MouseEvent>() {

						@Override
						public void handle(MouseEvent event) {
							int value;
							if (inputTextField.getText() == null) {
								value = 0;
							} else {
								value = Integer.valueOf(inputTextField.getText());
							}
							Platform.runLater(new Runnable() {

								@Override
								public void run() {

									playerLastInput = value;
									rightPlayerInput.getChildren().add(new Label(String.valueOf(value)));
									btnSubmit.setDisable(true);

									USER_MISSED_TURN = false;
								}
							});
							timer.interrupt();
							userTurnTask.interrupt();
						}

					});
				} else if (currentPlayer == 1) {
					Platform.runLater(new Runnable() {

						@Override
						public void run() {
							notification.setText("Current player is Computer");
							inputTextField.setText(null);
							inputTextField.setDisable(true);
							btnSubmit.setDisable(true);
						}
					});

					Thread computerTurnTask = new Thread(new Runnable() {

						@Override
						public void run() {
							int timeToThink = random.nextInt(TIME_CONSTRAINT);
							System.out.println("Need " + timeToThink + " to think");
							try {
								Thread.sleep(timeToThink);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							int value = random.nextInt(100);
							computerLastInput = value;
							Platform.runLater(new Runnable() {

								@Override
								public void run() {

									rightComputerInput.getChildren().add(new Label(String.valueOf(value)));
								}
							});
							timer.interrupt();
						}
					});
					computerTurnTask.setDaemon(true);
					computerTurnTask.start();
				}

				timer.setDaemon(true);
				timer.start();
				timer.join();

				if (currentPlayer == 2) {
					Platform.runLater(new Runnable() {

						@Override
						public void run() {
							rightSum.getChildren().add(new Label(String.valueOf(playerLastInput + computerLastInput)));
						}
					});

					turnIndex++;
					currentPlayer = 1;
				} else {
					currentPlayer = 2;
				}
			}
		}
	};

	private Thread gameManagerThread;

	@Override
	public void start(Stage primaryStage) throws Exception {
		gameState = new GameState();

		root = new BorderPane();

		top = new HBox();
		left = new VBox();
		right = new HBox();
		center = new HBox();
		bottom = new HBox();

		root.setLeft(left);
		root.setRight(right);
		root.setCenter(center);
		root.setTop(top);
		root.setBottom(bottom);

		center.setAlignment(Pos.TOP_CENTER);
		top.setAlignment(Pos.TOP_CENTER);
		left.setAlignment(Pos.TOP_CENTER);
		right.setAlignment(Pos.TOP_CENTER);
		bottom.setAlignment(Pos.TOP_CENTER);

		rightTurnIndex = new VBox();
		rightPlayerInput = new VBox();
		rightComputerInput = new VBox();
		rightSum = new VBox();

		timer = new ProgressBar();
		timer.progressProperty().bind(timerTask.progressProperty());
		timerLabel = new Label();
		timerLabel.textProperty().bind(timerTask.messageProperty());

		btnStart = new Button("Start");
		btnPause = new Button("Pause");
		btnContinue = new Button("Continue");
		btnSubmit = new Button("Submit");
		inputTextField = new TextField();
		notification = new Label();

		left.getChildren().addAll(timer, timerLabel);
		top.getChildren().addAll(btnStart, btnPause, btnContinue);
		center.getChildren().addAll(inputTextField, btnSubmit);
		bottom.getChildren().add(notification);
		right.getChildren().addAll(rightTurnIndex, rightPlayerInput, rightComputerInput, rightSum);

		HBox.setMargin(rightTurnIndex, new Insets(0, 50, 0, 0));
		HBox.setMargin(rightPlayerInput, new Insets(0, 50, 0, 0));
		HBox.setMargin(rightComputerInput, new Insets(0, 50, 0, 0));
		HBox.setMargin(rightSum, new Insets(0, 50, 0, 0));

		rightTurnIndex.getChildren().add(new Label("Turn #"));
		rightPlayerInput.getChildren().add(new Label("Player"));
		rightComputerInput.getChildren().add(new Label("Computer"));
		rightSum.getChildren().add(new Label("Sum"));

		btnStart.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				gameManagerThread = new Thread(gameManager);
				gameManagerThread.setDaemon(true);
				gameManagerThread.start();

				Thread timerTaskThread = new Thread(timerTask);
				timerTaskThread.setDaemon(true);
				timerTaskThread.start();

				btnStart.setDisable(true);
				btnContinue.setDisable(true);
			}
		});

		btnPause.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				gameState.setRunning(false);
				btnPause.setDisable(true);
				btnContinue.setDisable(false);
			}

		});

		btnContinue.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				gameState.setRunning(true);
				synchronized (gameState) {
					gameState.notifyAll();
				}

				btnPause.setDisable(false);
				btnContinue.setDisable(true);
			}
		});

		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
