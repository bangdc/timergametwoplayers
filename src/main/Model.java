package main;

import api.Player;
import api.GameState;

public class Model {
	Player player;
	Player computer;
	GameState gameState;

	public Model() {
		this.player = new Player("DO Cong Bang");
		this.computer = new Player("Computer");
		this.gameState = new GameState();
	}

	public Player getPlayer() {
		return player;
	}

	public Player getComputer() {
		return computer;
	}

	public GameState getGameState() {
		return gameState;
	}
}
