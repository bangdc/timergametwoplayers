package main;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class View {
	private BorderPane root;

	private HBox top;
	private VBox left;
	private HBox right;
	private HBox center;
	private HBox bottom;

	private ProgressBar timer;
	private Label timerLabel;
	private Button btnStart;
	private Button btnPause;
	private Button btnContinue;
	private Button btnSubmit;
	private TextField inputTextField;
	private Label notification;
	private VBox rightTurnIndex;
	private VBox rightPlayerInput;
	private VBox rightComputerInput;
	private VBox rightSum;

	public View() {
		root = new BorderPane();

		top = new HBox();
		left = new VBox();
		right = new HBox();
		center = new HBox();
		bottom = new HBox();

		root.setLeft(left);
		root.setRight(right);
		root.setCenter(center);
		root.setTop(top);
		root.setBottom(bottom);

		center.setAlignment(Pos.TOP_CENTER);
		top.setAlignment(Pos.TOP_CENTER);
		left.setAlignment(Pos.TOP_CENTER);
		right.setAlignment(Pos.TOP_CENTER);
		bottom.setAlignment(Pos.TOP_CENTER);

		rightTurnIndex = new VBox();
		rightPlayerInput = new VBox();
		rightComputerInput = new VBox();
		rightSum = new VBox();

		timer = new ProgressBar();
		timerLabel = new Label();

		btnStart = new Button("Start");
		btnPause = new Button("Pause");
		btnContinue = new Button("Continue");
		btnSubmit = new Button("Submit");
		inputTextField = new TextField();
		notification = new Label();

		left.getChildren().addAll(timer, timerLabel);
		top.getChildren().addAll(btnStart, btnPause, btnContinue);
		center.getChildren().addAll(inputTextField, btnSubmit);
		bottom.getChildren().add(notification);
		right.getChildren().addAll(rightTurnIndex, rightPlayerInput, rightComputerInput, rightSum);

		HBox.setMargin(rightTurnIndex, new Insets(0, 50, 0, 0));
		HBox.setMargin(rightPlayerInput, new Insets(0, 50, 0, 0));
		HBox.setMargin(rightComputerInput, new Insets(0, 50, 0, 0));
		HBox.setMargin(rightSum, new Insets(0, 50, 0, 0));

		rightTurnIndex.getChildren().add(new Label("Turn #"));
		rightPlayerInput.getChildren().add(new Label("Player"));
		rightComputerInput.getChildren().add(new Label("Computer"));
		rightSum.getChildren().add(new Label("Sum"));
	}

	public BorderPane getRoot() {
		return root;
	}

	public HBox getTop() {
		return top;
	}

	public VBox getLeft() {
		return left;
	}

	public HBox getRight() {
		return right;
	}

	public HBox getCenter() {
		return center;
	}

	public HBox getBottom() {
		return bottom;
	}

	public ProgressBar getTimer() {
		return timer;
	}

	public Label getTimerLabel() {
		return timerLabel;
	}

	public Button getBtnStart() {
		return btnStart;
	}

	public Button getBtnPause() {
		return btnPause;
	}

	public Button getBtnContinue() {
		return btnContinue;
	}

	public Button getBtnSubmit() {
		return btnSubmit;
	}

	public TextField getInputTextField() {
		return inputTextField;
	}

	public Label getNotification() {
		return notification;
	}

	public VBox getRightTurnIndex() {
		return rightTurnIndex;
	}

	public VBox getRightPlayerInput() {
		return rightPlayerInput;
	}

	public VBox getRightComputerInput() {
		return rightComputerInput;
	}

	public VBox getRightSum() {
		return rightSum;
	}
}
