package main;

import java.util.Random;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

public class Controller {
	private static int TIME_CONSTRAINT = 5000;
	private volatile static int TIMER_PROGRESS = 0;
	private volatile static boolean USER_MISSED_TURN = false;

	private View view;
	private Model model;

	Task<Void> timerTask;
	Task<Void> gameManager;

	public Controller(View view, Model model) {
		this.model = model;
		this.view = view;
	}

	public void init() {
		timerTask = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				while (true) {
					int progressSelf = TIMER_PROGRESS;
					updateProgress(progressSelf, 100);
					updateMessage(String.valueOf(progressSelf * TIME_CONSTRAINT));
				}
			}
		};

		gameManager = new Task<Void>() {
			Random random = new Random();

			@Override
			protected Void call() throws Exception {
				int turnIndex = 1;
				int currentPlayer = 1;

				while (true) {
					if (currentPlayer == 1) {
						System.out.println("Turn #" + turnIndex);
						int currentTurnIndex = turnIndex;
						Platform.runLater(new Runnable() {

							@Override
							public void run() {
								view.getRightTurnIndex().getChildren().add(new Label(String.valueOf(currentTurnIndex)));
							}
						});
					}

					Thread timer = new Thread(new Runnable() {

						@Override
						public void run() {
							for (int i = 100; i >= 0; i--) {
								synchronized (model.getGameState()) {
									while (!model.getGameState().isRunning()) {
										try {
											model.getGameState().wait();
										} catch (InterruptedException e) {
											e.printStackTrace();
										}
									}
								}

								TIMER_PROGRESS = i;
								try {
									Thread.sleep(TIME_CONSTRAINT / 100);
								} catch (InterruptedException e) {
									TIMER_PROGRESS = 0;
									break;
								}
							}
							System.out.println("TIME'S UP");
						}
					});

					if (currentPlayer == 2) {
						USER_MISSED_TURN = true;

						Platform.runLater(new Runnable() {

							@Override
							public void run() {
								view.getNotification().setText("Current player is Player");
								view.getInputTextField().setDisable(false);
								view.getBtnSubmit().setDisable(false);
							}
						});

						Thread userTurnTask = new Thread(new Runnable() {

							@Override
							public void run() {
								try {
									Thread.sleep(TIME_CONSTRAINT);
								} catch (InterruptedException e) {
									return;
								}
								if (USER_MISSED_TURN) {
									System.out.println("Missing");
									model.getPlayer().setLastInput(0);
									Platform.runLater(new Runnable() {

										@Override
										public void run() {

											view.getRightPlayerInput().getChildren().add(new Label("Missed - 0"));
										}
									});
								}
							}
						});
						userTurnTask.setDaemon(true);
						userTurnTask.start();

						view.getBtnSubmit().setOnMouseClicked(new EventHandler<MouseEvent>() {

							@Override
							public void handle(MouseEvent event) {
								int value;
								if (view.getInputTextField().getText() == null) {
									value = 0;
								} else {
									value = Integer.valueOf(view.getInputTextField().getText());
								}
								Platform.runLater(new Runnable() {

									@Override
									public void run() {

										model.getPlayer().setLastInput(value);
										view.getRightPlayerInput().getChildren().add(new Label(String.valueOf(value)));
										view.getBtnSubmit().setDisable(true);

										USER_MISSED_TURN = false;
									}
								});
								timer.interrupt();
								userTurnTask.interrupt();
							}

						});
					} else if (currentPlayer == 1) {
						Platform.runLater(new Runnable() {

							@Override
							public void run() {
								view.getNotification().setText("Current player is Computer");
								view.getInputTextField().setText(null);
								view.getInputTextField().setDisable(true);
								view.getBtnSubmit().setDisable(true);
							}
						});

						Thread computerTurnTask = new Thread(new Runnable() {

							@Override
							public void run() {
								int timeToThink = random.nextInt(TIME_CONSTRAINT);
								System.out.println("Need " + timeToThink + " to think");
								try {
									Thread.sleep(timeToThink);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								int value = random.nextInt(100);
								model.getComputer().setLastInput(value);
								Platform.runLater(new Runnable() {

									@Override
									public void run() {

										view.getRightComputerInput().getChildren()
												.add(new Label(String.valueOf(value)));
									}
								});
								timer.interrupt();
							}
						});
						computerTurnTask.setDaemon(true);
						computerTurnTask.start();
					}

					timer.setDaemon(true);
					timer.start();
					timer.join();

					if (currentPlayer == 2) {
						Platform.runLater(new Runnable() {

							@Override
							public void run() {
								view.getRightSum().getChildren().add(new Label(String.valueOf(
										model.getPlayer().getLastInput() + model.getComputer().getLastInput())));
							}
						});

						turnIndex++;
						currentPlayer = 1;
					} else {
						currentPlayer = 2;
					}
				}
			}
		};
		view.getTimer().progressProperty().bind(timerTask.progressProperty());
		view.getTimerLabel().textProperty().bind(timerTask.messageProperty());

		view.getBtnStart().setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				Thread gameManagerThread = new Thread(gameManager);
				gameManagerThread.setDaemon(true);
				gameManagerThread.start();

				Thread timerTaskThread = new Thread(timerTask);
				timerTaskThread.setDaemon(true);
				timerTaskThread.start();

				view.getBtnStart().setDisable(true);
				view.getBtnContinue().setDisable(true);
			}
		});
		view.getBtnPause().setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				model.getGameState().setRunning(false);
				view.getBtnPause().setDisable(true);
				view.getBtnContinue().setDisable(false);
			}

		});

		view.getBtnContinue().setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				model.getGameState().setRunning(true);
				synchronized (model.getGameState()) {
					model.getGameState().notifyAll();
				}

				view.getBtnPause().setDisable(false);
				view.getBtnContinue().setDisable(true);
			}
		});
	}
}
