package api;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private String name;
	private volatile int lastInput;
	private List<Integer> inputs;

	public Player(String name) {
		this.name = name;
		inputs = new ArrayList<Integer>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLastInput() {
		return lastInput;
	}

	public void setLastInput(int lastInput) {
		this.lastInput = lastInput;
		inputs.add(Integer.valueOf(lastInput));
	}

	public List<Integer> getInputs() {
		return inputs;
	}
}
