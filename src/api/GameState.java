package api;

public class GameState {
	private boolean running;

	public GameState() {
		running = true;
	}

	public boolean isRunning() {
		return running;
	}

	public synchronized void setRunning(boolean running) {
		this.running = running;
	}
}